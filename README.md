# Eureka Server

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Eureka Server Dashboard

![Eureka Server Dashboard](img/Eureka.png "Eureka Server Dashboard")